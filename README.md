# DevOps Tools Engineer

# Tópico 701: Engenharia de Software

## 701.1 Desenvolvimento de software moderno

### Descrição:

Os candidatos devem ser capazes de projetar soluções de software adequadas para ambientes de tempo de execução modernos. Os candidatos devem entender como os serviços lidam com a persistência de dados, sessões, informações de status, transações, concorrência, segurança, desempenho, disponibilidade, dimensionamento, balanceamento de carga, mensagens, monitoramento e APIs. Além disso, os candidatos devem entender as implicações do Agile e do DevOps no desenvolvimento de software.

### Áreas de conhecimento:

- Compreender e projetar aplicativos baseados em serviço
- Compreender conceitos e padrões comuns de API
- Compreender aspectos de armazenamento de dados, status de serviço e manipulação de sessões
- Software de design para execução em contêineres
- Projetar software a ser implantado nos serviços em nuvem
- Consciência dos riscos na migração e integração de software legado monolítico
- Entenda os riscos comuns de segurança de aplicativos e maneiras de mitigá-los
- Compreender o conceito de desenvolvimento ágil de software
- Compreender o conceito de DevOps e suas implicações para desenvolvedores e operadores de software

### Termos:

- [REST, JSON](Resumo/Rest_JSON.md)
- [Service Orientated Architectures (SOA)](Resumo/SOA.md)
- [Microservices](Resumo/Microservices.md)
- [Immutable servers](Resumo/Immutable_servers.md)
- [Loose coupling](Resumo/Loose_coupling.md)
- [Cross site scripting, SQL injections, verbose error reports, API authentication, consistent enforcement of transport encryption](Resumo/Rest_JSON.md)
- [CORS headers and CSRF tokens](Resumo/Rest_JSON.md)
- [ACID properties and CAP theorem](Resumo/Rest_JSON.md)

## 701.2 Componentes e plataformas padrão para software

### Descrição:

Os candidatos devem entender os serviços oferecidos por plataformas em nuvem comuns. Eles devem poder incluir esses serviços em suas arquiteturas de aplicativos e cadeias de ferramentas de implantação e entender as configurações de serviço necessárias. Os componentes de serviço do OpenStack são usados como uma implementação de referência.

### Áreas de conhecimento:

- Recursos e conceitos de armazenamento de objetos
- Recursos e conceitos de bancos de dados relacionais e NoSQL
- Recursos e conceitos de intermediários e filas de mensagens
- Recursos e conceitos de serviços de Big Data
- Recursos e conceitos de tempos de execução de aplicativos / PaaS
- Recursos e conceitos de redes de entrega de conteúdo

### Termos:

- OpenStack Swift
- OpenStack Trove
- OpenStack Zaqar
- CloudFoundry
- OpenShift

## 701.3 Gerenciamento do código fonte

### Descrição:

Os candidatos devem poder usar o Git para gerenciar e compartilhar o código-fonte. Isso inclui a criação e a contribuição para um repositório, bem como o uso de tags, ramificações e repositórios remotos. Além disso, o candidato deve conseguir mesclar arquivos e resolver conflitos de mesclagem.

### Áreas de conhecimento:

- Compreender os conceitos do Git e a estrutura do repositório
- Gerenciar arquivos em um repositório Git
- Gerenciar ramificações e tags
- Trabalhar com repositórios e ramificações remotas, bem como submódulos
- Mesclar arquivos e ramificações
- Conhecimento de SVN e CVS, incluindo conceitos de soluções SCM centralizadas e distribuídas

### Termos:

- git
- .gitignore

## 701.4 Integração Contínua e Entrega Contínua

### Descrição:

Os candidatos devem entender os princípios e componentes de um pipeline de integração contínua e entrega contínua. Os candidatos devem poder implementar um pipeline de CI / CD usando o Jenkins, incluindo acionar o pipeline de CI / CD, unidade em execução, testes de integração e aceitação, empacotar software e manipular a implantação dos artefatos de software testados. Este objetivo abrange o conjunto de recursos do Jenkins versão 2.0 ou posterior.

### Áreas de conhecimento:

- Compreender os conceitos de integração e entrega contínua
- Entenda os componentes de uma pipeline de CI / CD incluindo compilações, unidade, testes de integração e aceitação, gerenciamento de artefato, entrega e implantação
- Compreender as práticas recomendadas de implantação
- Entenda a arquitetura e os recursos do Jenkins, incluindo Jenkins Plugins, API do Jenkins, notificações e construções distribuídas
- Definir e executar tarefas no Jenkins, incluindo manipulação de parâmetros
- Impressões digitais, artefatos e repositórios de artefatos
- Entenda como o Jenkins modela os pipelines de entrega contínua e implementa uma pipeline de entrega contínua declarativa no Jenkins
- Conhecimento de possíveis modelos de autenticação e autorização
- Compreensão do plug-in Pipeline
- Entenda os recursos de importantes módulos Jenkins, como o plug-in de artefato de cópia, o plug-in de impressão digital, o Docker Pipeline, o plug-in de criação e publicação do Docker, o plug-in Git e o plug-in de credenciais
- Conhecimento do Artifactory e do Nexus

### Termos

- Step, Node, Stage
- Jenkins SDL
- Jenkinsfile
- Declarative Pipeline
- Blue-green and canary deployment

# Tópico 702: Gestão de Conteinêres

## 702.1 Uso de contêiner

### Descrição:

Os candidatos devem poder criar, compartilhar e operar contêineres do Docker. Isso inclui a criação de Dockerfiles, o uso de um registro do Docker, a criação e a interação com contêineres, bem como a conexão de contêineres a redes e volumes de armazenamento. Este objetivo abrange o conjunto de recursos do Docker versão 17.06 ou posterior.

### Áreas de conhecimento:

- Compreender a arquitetura do Docker
- Usar imagens existentes do Docker em um registro do Docker
- Criar Dockerfiles e criar imagens a partir do Dockerfiles
- Carregar imagens em um registro do Docker
- Operar e acessar contêineres do Docker
- Conectar contêiner a redes Docker
- Use volumes Docker para armazenamento em contêiner compartilhado e persistente

### Termos

- docker
- Dockerfile
- .dockerignore

## 702.2 Implantação e orquestração de contêineres
### Descrição:

Os candidatos devem poder executar e gerenciar vários contêineres que trabalham juntos para fornecer um serviço. Isso inclui a orquestração de contêineres do Docker usando o Docker Compose em conjunto com um cluster existente do Docker Swarm, bem como usando um cluster Kubernetes existente. Este objetivo abrange os conjuntos de recursos do Docker Compose versão 1.14 ou posterior, o Docker Swarm incluído no Docker 17.06 ou posterior e no Kubernetes 1.6 ou posterior.

### Áreas de conhecimento:

- Compreender o modelo de aplicativo do Docker Compose
- Criar e executar arquivos de composição do Docker (versão 3 ou posterior)
- Compreender a arquitetura e a funcionalidade do modo Docker Swarm
- Executar contêineres em um Docker Swarm, incluindo a definição de serviços, pilhas e o uso de segredos
- Entender a arquitetura e o modelo de aplicativo Kubernetes
- Definir e gerenciar um aplicativo baseado em contêiner para o Kubernetes, incluindo a definição de Implantações, Serviços, Conjuntos de réplicas e Pods

### Termos:

- docker-compose
- docker
- kubectl

## 702.3 Infra-estrutura de contêineres

### Descrição:

Os candidatos devem poder configurar um ambiente de tempo de execução para contêineres. Isso inclui a execução de contêineres em uma estação de trabalho local, bem como a configuração de um host de contêiner dedicado. Além disso, os candidatos devem estar cientes de outras infra-estruturas de contêiner, armazenamento, rede e aspectos de segurança específicos do contêiner. Este objetivo abrange o conjunto de recursos do Docker versão 17.06 ou posterior e do Docker Machine 0.12 ou posterior.

### Áreas de conhecimento:

- Usar o Docker Machine para configurar um host do Docker
- Compreender os conceitos de rede do Docker, incluindo redes de sobreposição
- Criar e gerenciar redes Docker
- Compreender os conceitos de armazenamento do Docker
- Criar e gerenciar volumes do Docker
- Conhecimento de Flocker e Flannel

### Termos:

• docker-machine

# Tópico 703: Implantação de Máquina

## 703.1 Uso de contêiner

### Descrição:

Os candidatos devem poder automatizar a implantação de uma máquina virtual com um sistema operacional e um conjunto específico de arquivos e software de configuração.

### Áreas de conhecimento:

- Compreender a arquitetura e os conceitos do Vagrant, incluindo armazenamento e rede
- Recuperar e usar caixas do Atlas
- Criar e executar arquivos Vagrant
- Acessar máquinas virtuais do Vagrant
- Compartilhe e sincronize a pasta entre uma máquina virtual Vagrant e o sistema host
- Entenda o provisionamento do Vagrant, incluindo File, Shell, Ansible e Docker
- Compreender a configuração de várias máquinas

### Termos:

- vagrant
- Vagrantfile

## 703.2 Implantação na Nuvem

### Descrição:

Os candidatos devem poder configurar instâncias da nuvem IaaS e ajustá-las para corresponder aos recursos de hardware disponíveis, especificamente espaço em disco e volumes. Além disso, os candidatos devem poder configurar instâncias para permitir logins SSH seguros e preparar as instâncias para estarem prontas para uma ferramenta de gerenciamento de configuração como Ansible.

### Áreas de conhecimento:

- Compreendendo os recursos e conceitos do cloud-init, incluindo dados do usuário e inicializando e configurando o cloud-init
- Usar o cloud-init para criar, redimensionar e montar sistemas de arquivos, configurar contas de usuário, incluindo credenciais de login como chaves SSH e instalar pacotes de software do repositório da distribuição
- Entenda os recursos e implicações das nuvens IaaS e da virtualização para uma instância de computação, como limites de instantâneo, pausa, clonagem e recursos.

## 703.3 Criação de imagem do sistema

### Descrição:

Os candidatos devem poder criar imagens para contêineres, máquinas virtuais e instâncias de nuvem IaaS.

### Áreas de conhecimento:

- Entenda a funcionalidade e os recursos do Packer
- Criar e manter arquivos de modelo
- Crie imagens a partir de arquivos de modelo usando diferentes construtores

### Termos:

- packer

# Tópico 704: Gerenciamento de configuração

## 704.1 Ansible

### Descrição:

Os candidatos devem poder usar o Ansible para garantir que um servidor de destino esteja em um estado específico em relação à sua configuração e software instalado. Este objetivo abrange o conjunto de recursos do Ansible versão 2.2 ou posterior.

### Áreas de conhecimento:

- Compreender os princípios de configuração automatizada do sistema e instalação de software
- Criar e manter arquivos de inventário
- Entender como o Ansible interage com sistemas remotos
- Gerenciar credenciais de logon SSH para Ansible, incluindo o uso de contas de logon não privilegiadas
- Criar, manter e executar manuais Ansible, incluindo tarefas, manipuladores, condicionais, loops e registros
- Definir e usar variáveis
- Mantenha segredos usando cofres Ansible
- Escreva modelos Jinja2, incluindo o uso de filtros, loops e condicionais comuns
- Compreender e usar funções Ansible e instalar funções Ansible do Ansible Galaxy
- Entenda e use tarefas Ansible importantes, incluindo arquivo, cópia, modelo, ini_file, lineinfile, patch, replace, user, group, command, shell, service, systemd, cron, apt, debconf, yum, git e debug
- Consciência do inventário dinâmico
- Recursos de reconhecimento de Ansibles para sistemas não Linux
- Conhecimento de recipientes Ansible

### Termos

- ansible.cfg
- ansible-playbook
- ansible-vault
- ansible-galaxy
- ansible-doc

## 704.2 Outras ferramentas de gerenciamento de configuração

### Descrição:

Os candidatos devem entender os principais recursos e princípios de importantes ferramentas de gerenciamento de configuração que não sejam o Ansible.

### Áreas de conhecimento:

- Conhecimentos básicos e de arquitetura do Puppet.
- Conhecimento básico e de arquitetura do Chef.

### Termos:

- Manifest, Class, Recipe, Cookbook
- puppet
- chef
- chef-solo
- chef-client
- chef-server-ctl
- knife

# Tópico 705: Operações de Serviço

## 705.1 Operações e monitoramento de TI

### Descrição

Os candidatos devem entender como a infraestrutura de TI está envolvida na entrega de um serviço. Isso inclui conhecimento sobre os principais objetivos das operações de TI, entendimento das propriedades funcionais e não funcionais de um serviço de TI e maneiras de monitorá-lo e medi-lo usando o Prometheus. Além disso, os candidatos devem entender os principais riscos de segurança na infraestrutura de TI. Este objetivo abrange o conjunto de recursos do Prometheus 1.7 ou posterior.

### Áreas de conhecimento:

- Compreender os objetivos das operações de TI e provisionamento de serviços, incluindo propriedades não funcionais, como disponibilidade, latência, capacidade de resposta
- Compreender e identificar métricas e indicadores para monitorar e medir a funcionalidade técnica de um serviço
- Compreender e identificar métricas e indicadores para monitorar e medir a funcionalidade lógica de um serviço
- Entenda a arquitetura do Prometheus, incluindo Exportadores, Pushgateway, Alertmanager e Grafana
- Monitore contêineres e microsserviços usando o Prometheus
- Compreender os princípios dos ataques de TI à infraestrutura de TI
- Mantenha segredos usando cofres Ansible
- Entenda os princípios das maneiras mais importantes de proteger a infraestrutura de TI
- Entenda os principais componentes da infraestrutura de TI e o papel deles na implantação

### Termos

- Prometheus, Node exporter, Pushgateway, Alertmanager, Grafana
- Service exploits, brute force attacks, and denial of service attacks
- Security updates, packet filtering and application gateways
- Virtualization hosts, DNS and load balancers

## 705.2 Gerenciamento e análise de logs

### Descrição:

Os candidatos devem entender o papel dos arquivos de log nas operações e na solução de problemas. Eles devem poder configurar uma infraestrutura de log centralizada com base no Logstash para coletar e normalizar dados de log. Além disso, os candidatos devem entender como o Elasticsearch e o Kibana ajudam a armazenar e acessar dados de log.nto de configuração que não sejam o Ansible.

### Áreas de conhecimento:

- Entenda como o log do aplicativo e do sistema funciona
- Entenda a arquitetura e a funcionalidade do Logstash, incluindo o ciclo de vida de uma mensagem de log e os plug-ins do Logstash
- Compreender a arquitetura e a funcionalidade do Elasticsearch e Kibana no contexto do gerenciamento de dados de log (Elastic Stack)
- Configure o Logstash para coletar, normalizar, transformar e armazenar dados de log
- Configure o syslog e o Filebeat para enviar dados de log para o Logstash
- Configure o Logstash para enviar alertas por email
- Compreender o suporte a aplicativos para gerenciamento de logs

### Termos:

- logstash
- input, filter, output
- grok filter
- Log files, metrics
- syslog.conf
- /etc/logstash/logstash.yml
- /etc/filebeat/filebeat.yml
